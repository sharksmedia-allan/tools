<?php
	class CSVExport {
		protected $data = [];
		protected $headerFields = [];

		private $fromCharset = "auto";

		private $delimiter;

		const CRLF = "\r\n";
		const TAB = "\t";
		const SEMICOLON = ";";
		const COMMA = ",";

		/**
		* Constructor
		* @param array $data - A multidimensional array representing the csv data
		* @param char $delimiter The cell delimiter
		*/
		public function __construct(array $data, $delimiter = self::TAB) {
			$this->data = $data;
			
			$this->delimiter = $delimiter;
		}

		/**
		* Use the keys of the first row as header keys
		* @return object $this
		*/
		public function useKeysAsHeader() {
			$this->headerFields = array_keys($this->data[0]);

			return $this;
		}

		/**
		* Use the first row as header instead of keys.
		*/
		public function useFirstRowAsHeader() {
			$this->headerFields = $this->data[0];
			unset($this->data[0]);

			return $this;
		}

		/**
		* Helper function to set delimiter, if not done in constructor
		* @param The cell delimiter
		* @return object $this
		*/
		public function setDelimiter($delimiter) {
			$this->delimiter = $delimiter;
		}

		/**
		* Set the charset in which the data should be converted from.
		* @param string $from  - The charset to convert from
		* @return object $this
		*/
		public function setCharset($from = "auto") {
			$this->fromCharset = $from;

			return $this;
		}

		/**
		* Get CSV data from constructors data array
		* @return string - The CSV data.
		*/
		public function getCSV() {
			$lines = [];
			
			if(count($this->headerFields) > 0) {
				$lines[] = implode($this->delimiter, $this->headerFields);
			}
			
			foreach($this->data as $lineData) {
				$lines[] = implode($this->delimiter, $lineData);
			}

			$csv = implode(self::CRLF, $lines);

			return chr(0xFF) . chr(0xFE) . mb_convert_encoding($csv, "UTF-16LE", $this->fromCharset);
		}

		/**
		* Sends the CSV data as a file to the browser
		* @param string $filename - Name of the file the browser recieves
		* @return object $this;
		*/
		public function sendFile($filename) {
			header("Content-Type: text/csv; charset=UTF-16LE");
			header("Content-Disposition: attachment; filename=" . basename($filename));
			header("Pragma: no-cache");
			header("Expires: 0");

			print $this->getCSV();

			return $this;
		}

		/**
		* Save the CSV data to a file on the filesyste,
		* @param string $location - Full system path where the file should be saved to
		* @return int - Number of bytes written
		*/
		public function saveFile($location) {
			$data = $this->getCSV();

			$fp = fopen($location, "w+");

			for ($written = 0; $written < mb_strlen($data); $written += $fwrite) {
				$fwrite = fwrite($fp, mb_substr($data, $written));

				if ($fwrite === false) {
					return $written;
				}
			}

			return $written;
		}
	}