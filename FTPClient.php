<?php
	class FTPClient {
		private $conn = null;
		private $host, $port, $timeout;
		private $use_ssl = false;
		private $authorixed = false;
		
		// The application which this class resides in, default error handler.
		// I've implemented my own so i'm able to throw regular errors instead.
		private $app_error_handler;

		const BINARY = FTP_BINARY;
		const ASCII = FTP_ASCII;

		public function __construct($host, $port = 21, $timeout = 90) {
			$this->host = $host;
			$this->port = $port;
			$this->timeout = $timeout;

			$this->app_error_handler = set_error_handler(array($this, "errorhandler"));
		}

		public function __destruct() {
			$this->close();
			restore_error_handler();
		}

		// Allows usage for any ftp_* functions in PHP not implemented by this class.
		public function __call($func, $a) {
			if(function_exists("ftp_".$func)) {
				array_unshift($a, $this->conn);
				return call_user_func_array("ftp_".$func, $a);
			} else {
				throw new Exception($func." is not a valid ftp function.");
			}
		} 

		public static function errorhandler($errno, $errstr, $errfile, $errline, $errcontext) {
			if (!(error_reporting() & $errno)) {
				// This error code is not included in error_reporting
				return;
			}
			if($errfile !== __FILE__ ) {
				// We only want errors from this class, let's not be bugged about everything else.
				return false;
			}

			throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
		}

		public function close() {
			if(is_resource($this->conn)) {
				ftp_close($this->conn);
			}
		}

		public function login($username, $password) {
			if($this->use_ssl === true) {
				if(!function_exists("ftp_ssl_connect")) {
					throw new Exception("Function ftp_ssl_connect() is not enabled on this server.");
				} else {
					$this->conn = ftp_ssl_connect($this->host, $this->port, $this->timeout);
				}
			} else {
				$this->conn = ftp_connect($this->host, $this->port, $this->timeout);
			}

			if(ftp_login($this->conn, $username, $password) !== false) {
				$this->authorized = true;
			}

			return $this;
		}

		public function ssl() {
			$this->use_ssl = true;
			return $this;
		}

		// TODO: There gotta be some way to optimize the way i use this bastard..
		public function nlist($directory = null) {
			if($directory === null) {
				$directory = $this->pwd();
			}
			if(is_array($children = ftp_rawlist($this->conn, $directory))) { 
				$items = array(); 
				foreach ($children as $child) { 
					$chunks = preg_split("/\s+/", $child); 
					list($item['rights'], $item['number'], $item['user'], $item['group'], $item['size'], $item['month'], $item['day'], $item['time']) = $chunks; 
					$item['type'] = $chunks[0]{0} === 'd' ? 'directory' : 'file'; 
					array_splice($chunks, 0, 8); 
					$items[implode(" ", $chunks)] = (object) $item; 
				} 

				return $items;
			}

        	// TODO: consider if I should fallback to ftp_nlist(); here..
        	// but for now this will have to do.
        	throw new Exception("Unable to parse the return raw_nlist() output.");
		}

		public function is_dir($dir) {
			$list = $this->nlist();
			return isset($list[$dir]) && $list[$dir]->type == "directory";
		}

		public function is_file($dir) {
			$list = $this->nlist();
			return isset($list[$dir]) && $list[$dir]->type == "file";
		}

		public function passive($mode = true) {
			if($this->authorized === false) {
				throw new Exception("You must login before adjusting passive mode.");
			}
			
			ftp_pasv($this->conn, $mode);
			return $this;
		}

		public function pwd() {
			return ftp_pwd($this->conn);
		}

		public function chdir($directory = '.') {
			ftp_chdir($this->conn, $directory);
			return $this;
		}

		public function chmod($mode, $filename) {
			ftp_chmod($this->conn, $mode, $filename);
			return $this;
		}

		public function mkdir($directory, $mode = 0740) {
			$directory = rtrim($directory, '/');
			$pwd = $this->pwd();

			// Find out whether we should create the directory recursively
			// TODO: Should i Optimize this function so it does not make calls 
			// to is_file and is_dir, to save performance and network traffic?
			if(strpos($directory, '/') !== false) {
				$diretories = explode('/', $directory);
				foreach($diretories as $new_directory) {
					if($this->is_file($new_directory)) {
						throw new Exception($new_directory." is a file..");
					}
					if(!$this->is_dir($new_directory)) {
						ftp_mkdir($this->conn, $new_directory);
						$this->chmod($mode, $new_directory);
					}
					$this->chdir($new_directory);
				}

				$this->chdir($pwd);
			} else {
				ftp_mkdir($this->conn, $directory);
				$this->chmod($mode, $directory);
			}

			return $this;
		}

		function put($source) {
			if(!is_readable($source)) {
				throw new Exception($source." is not readable.");
			}

			if(is_file($source)) {
				if(ftp_put($this->conn, basename($source), $source, self::BINARY) === false) {
					throw new Exception("Unable to put file ".$source);
				}
			}

			return $this;
		}

		public function get($remoteFilename, $overwrite = false) {
			$localFilename = getcwd().'/'.basename($remoteFilename);

			if(file_exists($localFilename) && $overwrite === false) {
				throw new Exception(basename($remoteFilename).' already exists at this location, and overwrite was not allowed.');
			}

			ftp_get($this->conn, $localFilename, $remoteFilename, self::BINARY, 0);
		}
	}