<?php
	/**
	* A simple class for easily logging a bunch of strings continously.
	*
	* Any data will be logged continously to get as much as possible
	* logged before any unexpected application crash.
	*
	* While writing this i made an exception to my normal coding standards.
	* I did not write this with performance in mind, because in the real world, we need at least something for debugging.
	* Performance and bigdata simply just contradicts each other.
	*
	* @author Allan Thue Rehhoff
	* @link http://rehhoff.me
	* @version 1.0
	*/
	class Logger {
		private $output, $log;
		private $history = [];

		const EMERGENCY = "EMERGENCY";
		const CRITICAL = "CRITICAL";
		const WARNING = "WARNING";
		const NOTICE = "NOTICE";
		const DEBUG = "DEBUG";

		const TRASHCAN = "/dev/null";

		/**
		* Use the constructer params to decide whether you want to output your log,
		* save it for later inspection, or both.
		* Please be aware that if you do not define the path to a log file all lines written will be discarded.
		*
		* @param (boolean) $out Whether lines to the log will be output at the time of logging.
		* @param (string) $log Path to the log file If the file does not exist an attempt to create it will be made.
		* @link http://php.net/manual/en/function.fopen.php More information at the PHP documentation site.
		*/
		public function __construct($out = true, $log = self::TRASHCAN) {
			$log = is_writable($log) ? $log : self::TRASHCAN;
			$this->output = $out;
			$this->log = fopen($log, 'c');
		}

		/**
		* Simply just closes the log file.
		* By now we should have no more need for that.
		*/
		public function __destruct() {
			if(is_resource($this->log)) {
				fclose($this->log);
			}
		}

		/**
		* Prints out the entire log so far
		*/
		public function __toString() {
			return implode("\r\n", $this->history);
		}

		/**
		* Truncate the current log file to zero length.
		* @return object
		* @throws Exception
		*/
		public function truncate() {
			if (flock($this->log, LOCK_EX)) {
				ftruncate($this->log, 0);
				flock($this->log, LOCK_UN);
				rewind($this->log);
				$this->history = [];
			} else {
				throw new Exception("Unable to acquire an exclusive lock.");
			}
			return $this;
		}

		/**
		* Adds a line to the log.
		* @param (string) $message the string to write.
		* @param (string) $severity Severity of the error/notice.
		* @return object
		*/
		public function write($message, $severity = self::NOTICE) {
			$datetime = new DateTime();
			$nextLineNumber = $this->nextLineNumber();
			$formattedMessage = '['.$datetime->format("Y-m-d h:i:sP")."] [".$severity."] ".$message."\r\n";

			$this->history[$nextLineNumber] = $formattedMessage;

			fwrite($this->log, $formattedMessage);

			if($this->output === true) {
				print $formattedMessage; // I'm most likely going to hate myself for this.
			}

			return $this;
		}

		/**
		* Loads a log file into the history.
		* WARNING! Calling this method wipes the current history, if any has been made.
		* @param (string) $filename A valid path to the logfile to load.
		* @param (boolean) $useFileAsLog Whether to use this file as the desired logfile.
		* @return object
		* @throws Exception
		*/
		public function load($filename, $useFileAsLog = true) {
			if(!is_readable($filename)) {
				throw new Exception($filename." is not a readable file.");
			}
			$this->history = [];
			$logContents = file_get_contents($filename);
			$logContentsToHistory = explode("\r\n", $logContents);
			foreach($logContentsToHistory as $line) {
				$this->history[$this->nextLineNumber()] = $line;
			}

			if($useFileAsLog === true) {
				if(!is_writable($filename)) {
					throw new Exception($filename, " is not writeable.");
				}
				$this->log = fopen($filename, 'a');
			} else {
				$this->log = fopen(self::TRASHCAN, 'c');
			}

			return $this;
		}

		/**
		* Get a line value by a given line number.
		* @param (int) $lineNumber Line number for which to fetch a value.
		* @return string
		*/
		public function lineValue($lineNumber) {
			return $this->history[$lineNumber];
		}

		/**
		* Retrieve the current line number in the log file.
		* @return (int) An integer representing the current line number.
		*/
		public function currentLineNumber() {
			end($this->history); // Is it just me, or does this seem edgy?
			return (!empty($this->history)) ? key($this->history) : 1;
		}

		/**
		* Retrieve the next line number to be used in the log file.
		* @return (int) An integer representing the next line number to be used.
		*/
		public function nextLineNumber() {
			return $this->currentLineNumber()+1;
		}

		/**
		* Adds a debug level message level to the log.
		* @param (string) $message A string of text.
		* @return object
		*/
		public function debug($message) {
			$this->write($message, self::DEBUG);
			return $this;
		}

		/**
		* Adds a notice level message to the log
		* @param (string) $message A string of text.
		* @return object
		*/
		public function notice($message) {
			$this->write($message, self::NOTICE);
			return $this;
		}

		/**
		* Adds a warning level message to the log
		* @param (string) $message A string of text.
		* @return object
		*/
		public function warning($message) {
			$this->write($message, self::WARNING);
			return $this;
		}

		/**
		* Adds a critical level message to the log
		* @param (string) $message A string of text.
		* @return object
		*/
		public function critical($message) {
			$this->write($message, self::CRITICAL);
			return $this;
		}

		/**
		* Adds an emergency level message to the log
		* @param (string) $message A string of text.
		* @return object
		*/
		public function emergency($message) {
			$this->write($message, self::EMERGENCY);
			return $this;
		}
	}