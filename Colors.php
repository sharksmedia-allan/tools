<?php
class Colors {
	/**
	* Converts a hexadecimal value to the corrosponding RGB color code
	* @param string $hexcode A hexadecimal color value.
	* @return int - A value that could be passed to Colors::RGBToHSL
	* @example
		$colour = '#FFFFFF';
		$rgb = HTMLToRGB($colour);
		$hsl = RGBToHSL($rgb);
		if($hsl->lightness > 200) {
			// this is light colour!
		}

	*/
	public static function HexToRGB($hexcode) {
		if($hexcode[0] == '#') {
			$hexcode = substr($hexcode, 1);
		}

		if (strlen($hexcode) == 3) {
			$hexcode = $hexcode[0] . $hexcode[0] . $hexcode[1] . $hexcode[1] . $hexcode[2] . $hexcode[2];
		}

		$r = hexdec($hexcode[0] . $hexcode[1]);
		$g = hexdec($hexcode[2] . $hexcode[3]);
		$b = hexdec($hexcode[4] . $hexcode[5]);

		return $b + ($g << 0x8) + ($r << 0x10);
	}

	/**
	* Takes an rgb value and calculates to HSL value
	* @param int $RGB - RGB value generated from Colors::HexToRGB();
	* @return object
	*/
	public static function RGBToHSL($RGB) {
		$r = 0xFF & ($RGB >> 0x10);
		$g = 0xFF & ($RGB >> 0x8);
		$b = 0xFF & $RGB;

		$r = ((float)$r) / 255.0;
		$g = ((float)$g) / 255.0;
		$b = ((float)$b) / 255.0;

		$maxC = max($r, $g, $b);
		$minC = min($r, $g, $b);

		$l = ($maxC + $minC) / 2.0;

		if($maxC == $minC) {
			$s = 0;
			$h = 0;
		} else {
			if($l < .5) {
				$s = ($maxC - $minC) / ($maxC + $minC);
			} else {
				$s = ($maxC - $minC) / (2.0 - $maxC - $minC);
			}

			if($r == $maxC) {
				$h = ($g - $b) / ($maxC - $minC);
			}

			if($g == $maxC) {
				$h = 2.0 + ($b - $r) / ($maxC - $minC);
			}

			if($b == $maxC) {
				$h = 4.0 + ($r - $g) / ($maxC - $minC);
			}

			$h = $h / 6.0; 
		}

		$h = (int)round(255.0 * $h);
		$s = (int)round(255.0 * $s);
		$l = (int)round(255.0 * $l);

		return (object) ["hue" => $h, "saturation" => $s, "lightness" => $l];
	}

	/**
	* @param string A hexadecimal color code.
	* @return string The inversed color value.
	*/
	public static function inverse($color) {
		$color = str_replace('#', '', $color);

		if (strlen($color) != 6) { return "#000000"; }

		$hex = '';
		for ($x=0; $x < 3; $x++) {
			$c = 255 - hexdec(substr($color, (2*$x), 2));
			$c = ($c < 0) ? 0 : dechex($c);
			$hex .= (strlen($c) < 2) ? '0'.$c : $c;
		}

		return '#'.$hex;
	}
}