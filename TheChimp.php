<?php
/*
* @author Allan Rehhoff
* @see https://apidocs.mailchimp.com/api/2.0/
* @version 2.0
*/
class TheChimp {
		protected $curl, $mcFields;
		private $endpoint;

		public function __construct() {
			$this->newConnection();
		}
		public function __destruct() {
			curl_close($this->curl);
		}

		public function setEndpoint($somewhere) {
			$this->endpoint = rtrim($somewhere, '/');
		}

		public function post($method, $fields) {
			$callUrl = $this->endpoint.$method;
			curl_setopt($this->curl, CURLOPT_URL, $callUrl);
			curl_setopt($this->curl, CURLOPT_POST, true);
			curl_setopt($this->curl, CURLOPT_POSTFIELDS, json_encode($fields));

			$response = curl_exec($this->curl);

			$this->newConnection();

			return json_decode($response);
		}

		public function get($method, $fields) {
			throw new Exception('This method have yet to be implemented :) ');
		}

		private function newConnection() {
			//Close any previous connections, if present.
			if(is_resource($this->curl)) {
				curl_close($this->curl);
			}

			$this->curl = curl_init();
			curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, true);

			return $this;
		}
	}

	$chimp = new TheChimp;
	$chimp->setEndpoint('https://<dc>.api.mailchimp.com/2.0/') //The datacenter is the first 3 characters before the first dot in your API key.

	$fields = array(
		'apikey' => 'YOUR API KEY',
		'id' => 'YOUR MAILCHIMP LIST ID'
	);

	// Look up the post endpoints and their fields at the documentation
	// The setEndpoint will removes any trailing slashes, so you need a starting slash in the parameter
	$response = $chimp->post('/lists/update-member', $fields);